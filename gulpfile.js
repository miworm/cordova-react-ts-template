const gulp = require('gulp');
const webpackStream = require('webpack-stream')(require('./webpack.config'));

gulp.task('deploy-js', () => gulp.src('./src/ts/**/*.tsx?$').pipe(webpackStream).pipe(gulp.dest('./www/js/')));

gulp.task('move-files', () => gulp.src(['./src/**/*.*', '!./src/ts/**/*.*']).pipe(gulp.dest('./www/')));
