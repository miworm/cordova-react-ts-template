// @ts-ignore
import React from 'react';
import {render} from 'react-dom';
import {AppComponent} from "./components/AppComponent";

function onDeviceReady() {
    render(<AppComponent deviceIsReady={true}></AppComponent>, document.getElementById('root'));
}

render(<AppComponent deviceIsReady={false}></AppComponent>, document.getElementById('root'));
document.addEventListener('deviceready', onDeviceReady, false);
