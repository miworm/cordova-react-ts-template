// @ts-ignore
import React, {Component} from 'react';

interface StateParagraphConfig {
    deviceIsReady: boolean;
}

export class StateParagraphComponent extends Component<StateParagraphConfig> {
    render() {
        if (this.props === undefined) {
            throw Error('Config for StateParagraphComponent can`t be undefined');
        }

        if (this.props.deviceIsReady === true) {
            return <p className="event received" style={{display: 'block'}}>Device is Ready</p>;
        } else {
            return <p className="event listening">Connecting to Device</p>;
        }
    }
}