// @ts-ignore
import React, {Component} from 'react';
import {StateParagraphComponent} from './StateParagrafComponent';

interface AppConfig {
    deviceIsReady: boolean;
}

export class AppComponent extends Component<AppConfig> {
    render() {
        return <div className="app">
            <h1>Apache Cordova</h1>
            <div id="deviceready" className="blink">
                <StateParagraphComponent deviceIsReady={this.props.deviceIsReady}></StateParagraphComponent>
            </div>
        </div>;
    }
}