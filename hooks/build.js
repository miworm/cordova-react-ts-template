console.log('Prebuild start...');
const run = require('node-run-cmd').run;
const weit = require('deasync').loopWhile;

let isBuildInProgress = true;

build();
weit(() => isBuildInProgress);

console.log('Prebuild done. \n');

async function build() {
    try {
        await run('npm run build', {verbose: true, shell: true});
    } catch (e) {
        process.exit();
    } finally {
        isBuildInProgress = false;
    }
}